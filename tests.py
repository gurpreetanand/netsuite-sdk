import unittest
from netsuite_sdk import Client
import os




class NetsuiteTestCase(unittest.TestCase):

    def setUp(self):
        """
                setting up netsuite client
        """
        CONSUMER_KEY = os.environ['CONSUMER_KEY']
        CONSUMER_SECRET = os.environ['CONSUMER_SECRET']
        TOKEN_ID = os.environ['TOKEN_ID']
        TOKEN_SECRET = os.environ['TOKEN_SECRET']
        nsAccountID = os.environ['NS_ACCOUNT_ID']
        ACCOUNT = os.environ['ACCOUNT']

        self.client = Client(consumer_key=CONSUMER_KEY,
                consumer_secret=CONSUMER_SECRET,
                account_id=ACCOUNT,
                token_key=TOKEN_ID,
                token_secret=TOKEN_SECRET,
                realm=nsAccountID)


    def test_Get_Entity(self):
        print("\n Testing get request \n")
        data = self.client.generic.get('inventoryItem', '1548')
        # self.assertTrue(is_json(data)), "Invalid json response"
        self.assertEqual(1548, data['internalid'])

    def test_Get_Entity_With_ExternalId(self):
        print("\n Testing get request with external id \n")
        data = self.client.generic.get('inventoryItem', 'cus101', True)
        self.assertEqual('cus101', data['externalId'])


    def test_Update_Entity(self):
        print("\n Testing patch request\n")
        update_Data = {
            'cost':121
        }
        status = self.client.generic.patch('inventoryItem','8004',update_Data)
        data = self.client.generic.get('inventoryItem','cus101',True)
        self.assertEqual(update_Data['cost'],data['cost'])


    def test_Update_Entity_With_ExternalId(self):
        print("\n Testing patch request with external id")
        update_Data = {
            'cost':1200
        }
        status = self.client.generic.patch('inventoryItem','cus101',update_Data,True)
        data = self.client.generic.get('inventoryItem','cus101',True)
        self.assertEqual(update_Data['cost'], data['cost'])

    def test_Create_Entity_With_ExternalId(self):
        print("\n Testing post request with external id")
        Item_data = {
            "cost": 100,
            "displayName": "Custom inventory Item 3",
            "taxSchedule": {
                "links": [
                    {
                        "rel": "self",
                        "href": "https://4141876-sb2.suitetalk.api.netsuite.com/rest/platform/v1/record/"
                    }
                ],
                "id": "2",
                "refName": "S2"
            },
            "salesDescription": "Custom Inventory Item created By gurpreet.singh@rubrik.com",
            "itemId": "CUS103-ITEM",
            "purchaseDescription": "HDD-FRU-4TB35-RI13",
            "purchasePriceVarianceAcct": {
                "links": [
                    {
                        "rel": "self",
                        "href": "https://4141876-sb2.suitetalk.api.netsuite.com/rest/platform/v1/record/account/271"
                    }
                ],
                "id": "271",
                "refName": "59150 Cost of Goods Sold : Products - Cost of Goods Sold : Unit Costs - COGS - Purchase Price Variance"
            }
        }

        status = self.client.generic.post('inventoryItem',Item_data,'cus103')

        data = self.client.generic.get('inventoryItem','cus103',True)
        self.assertEqual(Item_data['itemId'], data['itemId'])

    def test_Delete_Entity_With_ExternalId(self):
        print("\n Testing delete request with external id")
        status = self.client.generic.delete('inventoryItem','cus103',True)

        data = self.client.generic.get('inventoryItem','cus103')

        self.assertTrue(data['status']==400)

if __name__ == "__main__":
    unittest.main()