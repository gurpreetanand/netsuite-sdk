************
netsuite-sdk
************


A simple python package for netsuite integration with any python app.
Netsuite-sdk is a wrapper library written around netsuite's rest APIs.

    Creating a client::

        client = Client(consumer_key=CONSUMER_KEY,
        consumer_secret=CONSUMER_SECRET,
        account_id=ACCOUNT,
        token_key=TOKEN_ID,
        token_secret=TOKEN_SECRET,
        realm=nsAccountID)

Use Cases
    GET::

        client.generic.get('RecordName',internal_id)

        # True is used to indicate use of external id
        client.genric.get('RecordName',external_id,True)

        #Return Type =  JSON string

    CREATE::

        data = {}
        client.generic.post('RecordName','Internal_id',data)

        client.generic.post('RecordName','External_id',data,True)

        #Return Type = Boolean

    UPDATE::

        data = {}
        client.generic.patch('RecordName','Internal_id',data)

        client.generic.patch('RecordName','External_id',data,True)

        #Return Type = Boolean

    DELETE::

        client.generic.delete('inventoryItem','Internal_id')

        client.generic.delete('inventoryItem','External_id',True)

        #Return Type = Boolean
