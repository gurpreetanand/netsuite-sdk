from distutils.core import setup
setup(
  name = 'netsuite-sdk',         # How you named your package folder (MyLib)
  packages = ['netsuite_sdk'],   # Chose the same as "name"
  version = '0.0.3',      # Start with a small number and increase it with every change you make
  license='APACHE 2.0',        # Chose a license from here: https://help.github.com/articles/licensing-a-repository
  description = 'Netsuite REST API wrapper',   # Give a short description about your library
  author = ['MOHIT SINGH', 'GURPREET SINGH ANAND'],                   # Type in your name
  author_email = ['mohit.singh@evontech.com','gurpreet.anand@evontech.com'],      # Type in your E-Mail
  url = 'https://bitbucket.org/gurpreetanand/netsuite-sdk.git ',   # Provide either the link to your github or to your website
  # download_url = 'https://github.com/user/reponame/archive/v_01.tar.gz',    # I explain this later on
  keywords = ['NETSUITE', 'REST', 'API'],   # Keywords that define your package best
  install_requires=[            # I get to this in a second
          'oauth2','requests'

      ],
  classifiers=[
    'Development Status :: 3 - Alpha',      # Chose either "3 - Alpha", "4 - Beta" or "5 - Production/Stable" as the current state of your package
    'Intended Audience :: Developers',      # Define that your audience are developers
    'Topic :: Software Development :: Build Tools',
    'License :: OSI Approved :: Apache 2.0 License',   # Again, pick a license
    'Programming Language :: Python :: 3',      #Specify which pyhton versions that you want to support
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6',
  ],
)