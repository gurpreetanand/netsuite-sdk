import json
class InventoryItem(object):

    def __init__(self,client):
        self.url = 'record/inventoryItem'
        self.client = client

    def find_by_internal_id(self, id):
        """

        :param id: item id
        :return: json object about inventory item
        """
        request_type = "GET"

        self.url = "{}/{}".format(self.url, id)
        action_url = self.client.base_url + self.url

        headers = self.client.generate_headers(request_type,action_url)

        connection =  self.client.generate_request(request_type,action_url,headers)
        return json.loads(connection.text)


    def update_cost_by_id(self,id,cost=0):
        """

        :param id: item id
        :param cost: item cost
        :return: status code 204
        """
        data = {'cost': cost}

        return self.update_by_id(id,data)


    def update_by_id(self,id,data):
        """

        :param id: item id
        :param data: raw json data
        :return: status code 204
        """


        request_type = "PATCH"
        self.url = "{}/{}".format(self.url, id)
        action_url = self.client.base_url + self.url

        headers = self.client.generate_headers(request_type, action_url)

        connection = self.client.generate_request(request_type, action_url, headers,json.dumps(data))
        if(connection.status_code == 204):
            return True