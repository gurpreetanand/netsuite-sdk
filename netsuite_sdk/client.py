import oauth2 as oauth
import  requests, time,json

class Client(object):
    from .inventory import InventoryItem
    from .generic import Generic

    def __init__(self,**kwargs):
        self.base_url = self.generate_url(kwargs.get('account_id'))
        self.token_key = kwargs.get('token_key')
        self.token_secret = kwargs.get('token_secret')
        self.consumer_key   = kwargs.get('consumer_key')
        self.consumer_secret = kwargs.get('consumer_secret')
        self.realm = kwargs.get('realm')

        """
        headers can't be generated at the initiation of client because, headers varies on request type 
        """
        # self.headers = self.generate_headers()


        #attach inventoryItem
        self.inventoryItem = Client.InventoryItem(self)
        self.generic = Client.Generic(self)


    def generate_url(self,account_id):
        return 'https://{}.suitetalk.api.netsuite.com/rest/platform/v1/'.format(account_id)


    def generate_headers(self,request_type,url):
        token = oauth.Token(key=self.token_key,secret=self.token_secret)
        consumer = oauth.Consumer(key=self.consumer_key,secret=self.consumer_secret)
        http_method = request_type
        params = {
            'oauth_version': "1.0",
            'oauth_nonce': oauth.generate_nonce(),
            'oauth_timestamp': str(int(time.time())),
            'oauth_token': token.key,
            'oauth_consumer_key': consumer.key
        }
        req = oauth.Request(method=http_method,url=url,parameters=params)
        signature_method= oauth.SignatureMethod_HMAC_SHA1()
        req.sign_request(signature_method,consumer,token)
        header = req.to_header(self.realm)
        headery = header['Authorization'].encode('ascii','ignore')
        headerx = {"Authorization": headery,"Content-Type": "application/json"}

        return headerx

    def generate_request(self,request_type,url,headers,payload=None):

        connection = requests.request(request_type,url,data=payload,headers=headers)
        return connection


    def request_master(self,request_type,url,payload=None):
        action_url = self.base_url + url

        headers = self.generate_headers(request_type, action_url)

        if payload:
            connection = self.generate_request(request_type, action_url, headers,payload)
        else:
            connection = self.generate_request(request_type, action_url, headers)

        return connection
