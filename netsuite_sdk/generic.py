import json
class Generic(object):

    def __init__(self,client):
        self.url = 'record'
        self.client = client

    def post(self,record_name,data,ext_id=None):


        """

        :param record_name:
        :param data:
        :param ext_id:
        :return:
        """
        """
        to avoid incremental url repetition
        for eg. 'https://4141876-sb2.suitetalk.api.netsuite.com/rest/platform/v1/record/inventoryItem/8004/inventoryItem/eid:cus101'
        """

        self.url = 'record'
        request_type = "POST"



        """
        url gereration varies based on request type
        """
        if ext_id:
            request_type = "PUT"
            self.url = "{}/{}/eid:{}".format(self.url,record_name,ext_id)
        else:
            self.url = "{}/{}".format(self.url,record_name)

        connection = self.client.request_master(request_type,self.url,json.dumps(data))

        if (connection.status_code == 204):
            return True

        else:
            return  False

    def get(self,record_name,id,ext_id=False):
        """

        :param record_name: Name of the record
        :param id: internal id / external id
        :param ext_id: True if id is external
        :return: json response for netsuite api
        """
        self.url = 'record'
        request_type = "GET"

        if ext_id:
            self.url = "{}/{}/eid:{}".format(self.url,record_name,id)
        else:
            self.url = "{}/{}/{}".format(self.url,record_name,id)

        connection = self.client.request_master(request_type, self.url)


        return  json.loads(connection.text)


    def patch (self,record_name,id,data,ext_id=False):

        self.url = 'record'
        request_type = "PATCH"

        if ext_id:
            self.url = "{}/{}/eid:{}".format(self.url, record_name, id)
        else:
            self.url = "{}/{}/{}".format(self.url, record_name, id)

        connection = self.client.request_master(request_type, self.url,json.dumps(data))


        if (connection.status_code == 204):
            return True
        else:
            return False


    def delete(self,record_name,id,ext_id=False):

        self.url = 'record'
        request_type = "DELETE"

        if ext_id:
            self.url = "{}/{}/eid:{}".format(self.url, record_name, id)
        else:
            self.url = "{}/{}/{}".format(self.url, record_name, id)

        connection = self.client.request_master(request_type, self.url)


        if (connection.status_code == 204):
            return True
        else:
            return False



